package org.nisum.example.controllers;

import org.nisum.example.dto.NewUserDto;
import org.nisum.example.dto.OutputMessageDto;
import org.nisum.example.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/user")
public class UserRestController {

    @Autowired
    IUserService userService;

    @PostMapping("/create")
    public ResponseEntity<Object> userCreation(@Valid @RequestBody NewUserDto newUser) {
        OutputMessageDto outputMessage = new OutputMessageDto();
        try {
            outputMessage.setMessageType(OutputMessageDto.MessageType.OK);
            outputMessage.setData(userService.userCreator(newUser));
            outputMessage.setMessage("User created successfully");
            return ResponseEntity.ok().body(outputMessage);
        }
        catch (Exception ex) {
            outputMessage.setMessageType(OutputMessageDto.MessageType.ERROR);
            outputMessage.setMessage("An error has ocurred while creating the user");
            outputMessage.setDetail(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(outputMessage);
        }
    }
}
