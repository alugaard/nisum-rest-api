package org.nisum.example.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "USERS")
public class User implements UserDetails, IBaseModel<String> {
    @Id
    private String username;
    @Column(unique = true)
    private String name;
    @Column
    private String lastname;
    @Column
    private String password;
    @Column
    private boolean accountexpired;
    @Column
    private boolean accountlocked;
    @Column
    private boolean credentialexpired;
    @Column
    private boolean enabled;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public LocalDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(LocalDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Column
    private boolean isActive;
    @Column
    private LocalDateTime creationDate;
    @Column
    private LocalDateTime lastLogin;
    @Column
    private LocalDateTime modifiedDate;
    @Column
    private String token;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column
    private String email;

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
    private List<Phone> phones;

    private static final long serialVersionUID = 1L;

    public User() {
        super();
    }

    public User(String username, String password, boolean accountexpired, boolean accountlocked,
                boolean credentialexpired, boolean enabled) {
        super();
        this.username = username;
        this.password = password;
        this.accountexpired = accountexpired;
        this.accountlocked = accountlocked;
        this.credentialexpired = credentialexpired;
        this.enabled = enabled;
    }

    @Override
    public String getId() {
        return this.username;
    }

    @Override
    public void setId(String username) {
        this.username = UUID.randomUUID().toString();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return !accountexpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !accountlocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !credentialexpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public boolean isAccountexpired() {
        return accountexpired;
    }

    public void setAccountexpired(boolean accountexpired) {
        this.accountexpired = accountexpired;
    }

    public boolean isAccountlocked() {
        return accountlocked;
    }

    public void setAccountlocked(boolean accountlocked) {
        this.accountlocked = accountlocked;
    }

    public boolean isCredentialexpired() {
        return credentialexpired;
    }

    public void setCredentialexpired(boolean credentialexpired) {
        this.credentialexpired = credentialexpired;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", password='" + password + '\'' +
                ", accountexpired=" + accountexpired +
                ", accountlocked=" + accountlocked +
                ", credentialexpired=" + credentialexpired +
                ", enabled=" + enabled +
                '}';
    }
}