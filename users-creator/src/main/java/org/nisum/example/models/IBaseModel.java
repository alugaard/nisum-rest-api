package org.nisum.example.models;

public interface IBaseModel<K> {
    public K getId();

    public void setId(K id);
}
