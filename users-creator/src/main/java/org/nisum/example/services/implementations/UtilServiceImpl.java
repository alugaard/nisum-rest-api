package org.nisum.example.services.implementations;

import org.nisum.example.services.IUserService;
import org.nisum.example.services.IUtilService;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UtilServiceImpl implements IUtilService {
    @Override
    public boolean emailValidator(String email) {
        String regex = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$";
        return email.matches(regex);
    }

    @Override
    public boolean passwordValidator(String password) {
        String regexUppercase = "[A-Z]";
        Pattern patternUppercase = Pattern.compile(regexUppercase);
        Matcher matcherUppercase = patternUppercase.matcher(password);

        int countUppercase = 0;
        while (matcherUppercase.find())
            countUppercase++;

        String regexDigits = "[\\d]";
        Pattern patternDigits = Pattern.compile(regexDigits);
        Matcher matcherDigits = patternDigits.matcher(password);

        int countDigits = 0;
        while (matcherDigits.find())
            countDigits++;

        String regexNotValid = "[^A-Za-z0-9]+";
        Pattern patternNotValid = Pattern.compile(regexNotValid);
        Matcher matcherNotValid = patternNotValid.matcher(password);

        int countNotValid = 0;
        while (matcherNotValid.find())
            countNotValid++;

        if(countUppercase == 1 && countDigits == 2 && countNotValid == 0){
            return true;
        }
        else{
            return false;
        }
    }
}
