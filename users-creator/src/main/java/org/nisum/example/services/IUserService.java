package org.nisum.example.services;

import org.nisum.example.dto.NewUserDto;
import org.nisum.example.models.User;

public interface IUserService {
    User userCreator(NewUserDto newUser) throws Exception;
}
