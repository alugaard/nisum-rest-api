package org.nisum.example.services;

public interface IUtilService {
    boolean emailValidator(String email);
    boolean passwordValidator(String password);
}
