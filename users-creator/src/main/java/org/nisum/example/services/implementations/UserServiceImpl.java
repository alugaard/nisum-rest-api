package org.nisum.example.services.implementations;

import org.nisum.example.dto.NewUserDto;
import org.nisum.example.dto.PhoneDto;
import org.nisum.example.models.Phone;
import org.nisum.example.models.User;
import org.nisum.example.repositories.IUserRepository;
import org.nisum.example.services.IUserService;
import org.nisum.example.services.IUtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    IUserRepository userRepository;
    @Autowired
    IUtilService utilService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User userCreator(NewUserDto newUser) throws Exception{
        validateNewUser(newUser);
        List<User> users = userRepository.findByEmail(newUser.getEmail());
        if(users.size() > 0){
            throw new Exception("the email already exists in database");
        }
        LocalDateTime currentTime = LocalDateTime.now();
        User user = new User();
        user.setAccountexpired(false);
        user.setAccountlocked(false);
        user.setActive(true);
        user.setCreationDate(currentTime);
        user.setCredentialexpired(false);
        user.setEmail(newUser.getEmail());
        user.setEnabled(true);
        user.setId("uuid");
        user.setLastLogin(currentTime);
        user.setModifiedDate(currentTime);
        user.setName(newUser.getName());
        user.setPassword(passwordEncoder.encode(newUser.getPassword()));
        user.setToken(user.getId());

        List<Phone> phones = new ArrayList<Phone>();
        for (PhoneDto p : newUser.getPhones()) {
            Phone newPhone = new Phone();
            newPhone.setCityCode(p.getCityCode());
            newPhone.setCountryCode(p.getCountryCode());
            newPhone.setNumber(p.getNumber());
            newPhone.setUser(user);
            phones.add(newPhone);
        }

        user.setPhones(phones);

        userRepository.save(user);
        return userRepository.findById(user.getId()).get();
    }

    private void validateNewUser(NewUserDto newUser) throws Exception{
        String message = "";
        if(!utilService.emailValidator(newUser.getEmail().trim())){
            message += "-> email not valid\n";
        }
        if(!utilService.passwordValidator(newUser.getPassword().trim())){
            message += "-> password not valid, must contain 1 capital letter and 2 digits and no special characters\n";
        }
        if(!message.equals("")){
            throw new Exception(message);
        }
    }
}
