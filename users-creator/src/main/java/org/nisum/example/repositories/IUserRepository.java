package org.nisum.example.repositories;

import org.nisum.example.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserRepository extends CrudRepository<User, String> {
    @Query(value = "SELECT U FROM User U WHERE U.email = :email", nativeQuery = false)
    List<User> findByEmail(@Param(value = "email") String email);
}
