package org.nisum.example.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class NewUserDto {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<PhoneDto> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneDto> phones) {
        this.phones = phones;
    }

    @NotNull(message = "name cannot be empty")
    private String name;
    @NotNull(message = "email cannot be empty")
    private String email;
    @NotNull(message = "password cannot be empty")
    @Size(min = 8, max = 12, message = "password should have between 8 and 12 characteres")
    private String password;
    @NotNull(message = "phones cannot be empty")
    @Size(min = 1, message = "at least one phone is necessary")
    private List<@Valid PhoneDto> phones;
}
