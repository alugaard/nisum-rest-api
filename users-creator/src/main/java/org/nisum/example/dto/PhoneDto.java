package org.nisum.example.dto;

import javax.validation.constraints.NotNull;

public class PhoneDto {
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getCityCode() {
        return cityCode;
    }

    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    @NotNull(message = "number cannot be empty")
    private Integer number;
    @NotNull(message = "cityCode cannot be empty")
    private Integer cityCode;
    @NotNull(message = "countryCode cannot be empty")
    private Integer countryCode;
}
