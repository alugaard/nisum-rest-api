package org.nisum.example.dto;

public class OutputMessageDto {
    public static enum MessageType {
        OK,
        WARNING,
        ERROR
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    private MessageType messageType;
    private String message;
    private String detail;
    private Object data;
}
